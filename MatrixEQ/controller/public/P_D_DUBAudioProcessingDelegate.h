//
//  P_D_DUBAudioProcessingDelegate.h
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol P_D_DUBAudioProcessingDelegate <NSObject>

/*
 * NSSet<DUBFrequencyChange>* frequencyChanges
 */
- (void)update:(NSSet*) frequencyChanges;

@end
