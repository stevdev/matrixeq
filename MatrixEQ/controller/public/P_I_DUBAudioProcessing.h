//
//  P_I_AudioProcessing.h
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol P_D_DUBAudioProcessingDelegate;

@protocol P_I_DUBAudioProcessing <NSObject>

@property (nonatomic, assign) id<P_D_DUBAudioProcessingDelegate> delegate;

- (void)start;

- (void)stop;

- (void)setValue:(float)value
    forFrequency:(NSUInteger)frequency;

@end
