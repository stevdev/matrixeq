//
//  DUBOSCMatrixSender.h
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "P_I_DUBMatrixSender.h"

@interface DUBOSCMatrixSender : NSObject <P_I_DUBMatrixSender>

@end
