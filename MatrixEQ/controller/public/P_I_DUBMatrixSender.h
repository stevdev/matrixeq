//
//  P_I_DUBMatrixSender.h
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol P_I_DUBMatrixSender <NSObject>

/*
 * matrix: two dimensional array of DUBPixel
 */
- (void)sendMatrix:(NSArray*)matrix
            toHost:(NSString*)hostIP
            onPort:(NSUInteger)port;

@end
