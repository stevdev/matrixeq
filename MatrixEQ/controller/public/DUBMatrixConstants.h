//
//  DUBMatrixConstants.h
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define FREQUENCY_TABLE @[@32, @64, @125, @250, @500, @1000, @2000, @4000, @8000, @16000]

#define ROWS                    8
#define LEDS_PER_ROW            8
#define PIXEL_SETS              2
#define INTS_PER_SET            16
#define PIXELS_ENCODED_IN_INT   2
#define PIXELS                  INTS_PER_SET * PIXELS_ENCODED_IN_INT

#define SET_1_ID 1//2^0
#define SET_2_ID 2//2^1

#define TARGET_PORT 8888
#define TARGET_IP   @"192.168.42.177"
