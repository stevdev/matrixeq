//
//  DUBOSCMatrixSender.m
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DUBOSCMatrixSender.h"
#import "DUBMatrixConstants.h"
#import "DUBMatrixUtils.h"
#import "DUBPixel.h"
#import "DUBColorEncoder.h"
#import "OSCManager.h"
#import "OSCMessage.h"
#import "OSCOutPort.h"
#import <math.h>

@interface DUBOSCMatrixSender ()

@property (nonatomic, retain) OSCManager *manager;
@property (nonatomic, retain) OSCOutPort *outPort;

@end

#pragma mark -

@implementation DUBOSCMatrixSender

@synthesize manager, outPort;

- (id)init {
    self = [super init];
    
    if (self) {
        self.manager = [[[OSCManager alloc] init] autorelease];
    }
    
    return self;
}

- (void)dealloc {
    
    self.manager = nil;
    
    [super dealloc];
}

#pragma mark -

- (void)sendMatrix:(NSArray*)matrix
            toHost:(NSString*)hostIP
            onPort:(NSUInteger)port {
    
    if (!self.outPort) {
        self.outPort = [OSCOutPort createWithAddress:hostIP 
                                             andPort:port];
    }
    
    NSArray *serializedMatrix = [DUBMatrixUtils serializeMatrix:matrix];
    
    for (uint8_t set = 0; set < PIXEL_SETS; set++) {
        
        OSCMessage *msg = [OSCMessage createWithAddress:@"/pxlset"];
        
        uint8_t setID = (uint8_t) pow(2, set);
        
        [msg addInt:setID];
        
        for (int int_ = 0; int_ < INTS_PER_SET; int_++) {
            
            int offset = PIXELS_ENCODED_IN_INT * (set * INTS_PER_SET + int_);
            
            DUBPixel *pxl1 			= [serializedMatrix objectAtIndex:offset];
            DUBPixel *pxl2 			= [serializedMatrix objectAtIndex:offset + 1];
            int32_t encodedPixels 	= [DUBColorEncoder encodePixel1:pxl1 
                                                          andPixel2:pxl2];
            [msg addInt:encodedPixels];
        }
        
        [self.outPort sendThisMessage:msg];
    }
}

@end
