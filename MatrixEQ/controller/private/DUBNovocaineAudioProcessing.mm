//
//  DUBNovocaineAudioProcessing.m
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DUBNovocaineAudioProcessing.h"
#import "P_D_DUBAudioProcessingDelegate.h"
#import "DUBFrequencyChange.h"
#import "NVSoundLevelMeter.h"
#import "NVPeakingEQFilter.h"
#import "DUBMatrixConstants.h"

#import <AVFoundation/AVFoundation.h>

@interface DUBNovocaineAudioProcessing ()

@property (nonatomic, retain) NVSoundLevelMeter *outputWatcher;

@property (nonatomic, retain) NSArray *frequencyTable;
@property (nonatomic, retain) NSMutableDictionary *filters;

@property (nonatomic, retain) NSMutableDictionary *scaleFactors;

@end

#pragma mark -

@implementation DUBNovocaineAudioProcessing

@synthesize outputWatcher, delegate, filters, frequencyTable, scaleFactors;

- (void)start {
    
    ringBuffer          = new RingBuffer(32768, 2); 
    audioManager        = [Novocaine audioManager];
    self.outputWatcher  = [[NVSoundLevelMeter alloc] init];
    
    self.frequencyTable = FREQUENCY_TABLE;
    
    self.scaleFactors   = [NSMutableDictionary dictionaryWithCapacity:[self.frequencyTable count]];
    self.filters        = [NSMutableDictionary dictionaryWithCapacity:[self.frequencyTable count]];
    
    for (NSNumber *frequency in self.frequencyTable) {
        
        NVPeakingEQFilter *pef = [[NVPeakingEQFilter alloc] initWithSamplingRate:audioManager.samplingRate];
        pef.centerFrequency = [frequency floatValue];
        pef.Q = 2.0f;
        
        if ([frequency intValue] == 500) {
            pef.G = 50.0f;
        } else if ([frequency intValue] == 3000) {
            pef.G = 20.0f;
        } else if ([frequency intValue] == 16000) {
            pef.G = 0.0f;
        } else if ([frequency intValue] == 1000) {
            pef.G = 80.0f;
        } else {
            pef.G = 0.0f;
        }
        
        [self.filters setObject:pef forKey:frequency];
        [self.scaleFactors setObject:@1 forKey:frequency];
    }
    
    [audioManager setInputBlock:^(float *data, UInt32 numFrames, UInt32 numChannels) {
        
        @autoreleasepool {

            NSMutableSet *frequencyChanges = [NSMutableSet setWithCapacity:[self.filters count]];
        
            float outputLevel       = 0;
            DUBFrequencyChange *fc  = nil;
        
            @synchronized(self.filters) {
                for (NVPeakingEQFilter *pef in [self.filters allValues]) {
                
                    [pef filterData:data numFrames:numFrames numChannels:numChannels];
                
                    outputLevel = [self.outputWatcher getdBLevel:data numFrames:numFrames numChannels:numChannels];
                
                    float scaleFactor = [[self.scaleFactors objectForKey:@(pef.centerFrequency)] floatValue];
                    
                    fc              = [[DUBFrequencyChange alloc] init];
                    fc.frequency    = pef.centerFrequency;
                    fc.newValue     = outputLevel * scaleFactor;
                
                    [frequencyChanges addObject:fc];
                }
            
                [self.delegate update:frequencyChanges];
            }
        }
    }];
}

- (void)stop {

}

- (void)setValue:(float)value
    forFrequency:(NSUInteger)frequency {
    
    @synchronized(self.filters) {
//        NVPeakingEQFilter *pef = [self.filters objectForKey:[NSNumber numberWithInteger:frequency]];
//        if (pef != nil) {
//            pef.G = value;
//        }
        
        [self.scaleFactors setObject:@(value) forKey:@(frequency)];
    }
}

@end
