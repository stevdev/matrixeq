//
//  DUBNovocaineAudioProcessing.h
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "P_I_DUBAudioProcessing.h"
#import "RingBuffer.h"
#import "Novocaine.h"

@interface DUBNovocaineAudioProcessing : NSObject <P_I_DUBAudioProcessing> {

    RingBuffer *ringBuffer;
    Novocaine *audioManager;
}

@end
