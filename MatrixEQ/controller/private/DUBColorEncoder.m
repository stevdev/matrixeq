//
//  DUBColorEncoder.m
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DUBColorEncoder.h"
#import "DUBPixel.h"

@implementation DUBColorEncoder


+ (int32_t)encodePixel1:(DUBPixel*)pxl1
              andPixel2:(DUBPixel*)pxl2 {
        
    int32_t color1 = [DUBColorEncoder encodePixel:pxl1];
    int32_t color2 = [DUBColorEncoder encodePixel:pxl2];
    
    color1 <<= 15;
    color1 |= color2;
    
    return color1;
}

+ (int32_t)encodePixel:(DUBPixel*)pxl {
    
    int32_t color = 0;
    
    color |= pxl.red;
    color <<= 5;
    color |= pxl.green;
    color <<= 5;
    color |= pxl.blue;
    
    return color;
}

@end
