//
//  DUBColorEncoder.h
//  OSCControl
//
//  Created by steven reinisch on 9/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DUBPixel;

@interface DUBColorEncoder : NSObject

+ (int32_t)encodePixel1:(DUBPixel*)pxl1
              andPixel2:(DUBPixel*)pxl2;

+ (int32_t)encodePixel:(DUBPixel*)pxl;

@end
