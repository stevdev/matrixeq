//
//  DUBAppDelegate.h
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol P_I_DUBAudioProcessing;
@class DUBViewController;

@interface DUBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DUBViewController *viewController;

@property (nonatomic, strong) id<P_I_DUBAudioProcessing> audio;

@end
