//
//  main.m
//  MatrixEQ
//
//  Created by steffen on 2/10/13.
//  Copyright (c) 2013 dubmas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DUBAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DUBAppDelegate class]));
    }
}
