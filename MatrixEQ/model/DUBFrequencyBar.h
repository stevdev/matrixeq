//
//  DUBFrequencyBar.h
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DUBFrequencyBar : NSObject

@property (nonatomic, assign) NSUInteger  frequency;
@property (nonatomic, assign) float_t     height;

@end
