//
//  DUBViewController.m
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DUBViewController.h"
#import "DUBFrequencyChange.h"
#import "DUBFrequencyBar.h"
#import "DUBGraphicalEQView.h"

#import "DUBMatrixConstants.h"
#import "P_I_DUBMatrixSender.h"
#import "DUBOSCMatrixSender.h"
#import "DUBMatrixUtils.h"
#import "DUBPixel.h"

#define MAX_BAR_HEIGHT 200
#define MIN_LEVEL -100
#define MAX_LEVEL 10


@interface DUBViewController ()

@property (nonatomic, retain) id<P_I_DUBMatrixSender> sender;

@property (nonatomic, assign) BOOL acceptState;

- (DUBFrequencyBar*)_toFrequencyBar:(DUBFrequencyChange*)change;

- (float_t)_barHeightForValue:(float_t)actualValue
                     maxValue:(float_t)max
                     minValue:(float_t)min;

- (void)_timerFinished:(NSTimer*)timer;

- (void)_drawBars:(NSArray*)bars withNumOfPixels:(NSArray*)numOfPixelsForBars;

@end

#pragma mark -

@implementation DUBViewController

@synthesize acceptState, sender;

- (void)dealloc {
    
    self.sender = nil;
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.sender = [[[DUBOSCMatrixSender alloc] init] autorelease];
    
	self.acceptState = YES;
    
    self.view = [[[DUBGraphicalEQView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)] autorelease];
    
    [NSTimer scheduledTimerWithTimeInterval:0.07
                                     target:self
                                   selector:@selector(_timerFinished:)
                                   userInfo:nil
                                    repeats:YES];
    
    /*
     * Clear the matrix
     */
    NSArray *matrix = [DUBMatrixUtils matrix];
    [DUBMatrixUtils clearMatrix:matrix];
    [self.sender sendMatrix:matrix
                     toHost:TARGET_IP
                     onPort:TARGET_PORT];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - P_D_DUBAudioProcessingDelegate

//- (void)update:(NSSet*) frequencyChanges {
//    
//    if (self.acceptState) {
//        
//        NSMutableSet *bars = [NSMutableSet setWithCapacity:[frequencyChanges count]];
//        
//        for (DUBFrequencyChange *change in frequencyChanges) {
//            [bars addObject:[self _toFrequencyBar:change]];
//        }
//        
//        self.acceptState = NO;
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [(DUBGraphicalEQView*)self.view drawBars:bars];
//        });
//    }
//}

/*
 * NSSet<DUBFrequencyChange>* frequencyChanges
 */
- (void)update:(NSSet*) frequencyChanges {
    
    if (self.acceptState) {
        
        self.acceptState = NO;
        
        NSMutableArray *bars = [NSMutableArray arrayWithCapacity:[frequencyChanges count]];
    
        NSArray *sortDescs = [NSArray arrayWithObject:
                              [NSSortDescriptor sortDescriptorWithKey:@"frequency"
                                                            ascending:YES]];
        
        for (DUBFrequencyChange *change in [frequencyChanges sortedArrayUsingDescriptors:sortDescs]) {
            [bars addObject:[self _toFrequencyBar:change]];
        }
    
        /*
         * Calculate how many pixels one bar will occupy on the 8x8 matrix
         * and store the numOfPixels in numOfPixelsForBars.
         * The number of pixels for a bar at bars[index] is retrieved by
         * numOfPixelsForBars[index].
         */
        
        NSMutableArray *numOfPixelsForBars  = [NSMutableArray arrayWithCapacity:[bars count]];
        NSUInteger numOfPixels              = 0;
        
        for (DUBFrequencyBar *bar in bars) {
            numOfPixels = bar.height / (MAX_BAR_HEIGHT / LEDS_PER_ROW);
            [numOfPixelsForBars addObject:[NSNumber numberWithUnsignedInteger:numOfPixels]];
        }
        
        [self _drawBars:bars withNumOfPixels:numOfPixelsForBars];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [(DUBGraphicalEQView*)self.view drawBars:bars];
        });
    }
}

#pragma mark - private

- (void)_drawBars:(NSArray*)bars withNumOfPixels:(NSArray*)numOfPixelsForBars {

    NSArray *matrix = [DUBMatrixUtils matrixWithPixelInitBlock:^(DUBPixel *pxl, uint8_t row, uint8_t led) {
        
        //NSLog(@"%d:%d", row, led);
        
        NSUInteger numOfPixels = [[numOfPixelsForBars objectAtIndex:led] unsignedIntegerValue];
        if (numOfPixels
            >=
            (ROWS - row)
            ) //should a pixel be drawn in the current row?
        {
            
            if (row == 0) { //at the peak
                pxl.red     = 31;
                pxl.green   = 0;
                pxl.blue    = 0;
                
            } else if (row == 1) { //one level below the peak
                pxl.red     = 31;
                pxl.green   = 24;
                pxl.blue    = 0;
                
            } else if (row == 2) {
                pxl.red     = 20;
                pxl.green   = 31;
                pxl.blue    = 0;
                
            } else if (row == 3) {
                pxl.red     = 10;
                pxl.green   = 31;
                pxl.blue    = 0;
                
            } else if (row == 4) {
                pxl.red     = 0;
                pxl.green   = 20;
                pxl.blue    = 10;
                
            } else if (row == 5) {
                pxl.red     = 0;
                pxl.green   = 10;
                pxl.blue    = 20;
                
            } else if (row == 6) {
                pxl.red     = 10;
                pxl.green   = 10;
                pxl.blue    = 20;
                
            } else if (row == 7) {
                pxl.red     = 10;
                pxl.green   = 20;
                pxl.blue    = 20;
                
            } else {
                
                pxl.red     = 31;
                pxl.green   = 31;
                pxl.blue    = 31;
                
            }
            
        }
        else {
            
            //no pixel should be drawn at this row ... make it black!
            pxl.red     = 0;
            pxl.green   = 0;
            pxl.blue    = 0;
        }
    }];
    
    [self.sender sendMatrix:matrix
                     toHost:TARGET_IP
                     onPort:TARGET_PORT];
}

- (DUBFrequencyBar*)_toFrequencyBar:(DUBFrequencyChange*)change {
    
    float_t barHeight = [self _barHeightForValue:change.newValue 
                                        maxValue:MAX_LEVEL 
                                        minValue:MIN_LEVEL];

    DUBFrequencyBar *bar    = [[[DUBFrequencyBar alloc] init] autorelease];
    bar.frequency           = change.frequency;
    bar.height              = barHeight;
        
    return bar;
}

- (float_t)_barHeightForValue:(float_t)actualValue
                     maxValue:(float_t)max
                     minValue:(float_t)min {
    
    float_t transform   = 0 - min;
    min                 = min + transform;
    max                 = max + transform;
    actualValue         = actualValue + transform;
    
    float_t percentageOfCompleteBar = (actualValue * 100) / max;
    
    return (MAX_BAR_HEIGHT * percentageOfCompleteBar) / 100;
}

- (void)_timerFinished:(NSTimer*)timer {
    self.acceptState = !self.acceptState;
}

@end
