//
//  DUBGraphicalEQView.m
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DUBGraphicalEQView.h"
#import "DUBFrequencyBar.h"
#import "DUBMatrixConstants.h"
#import "DUBAppDelegate.h"
#import "P_I_DUBAudioProcessing.h"

#define BAR_WIDTH 20.0
#define BAR_DISTANCE 10.0
#define BAR_OFFSET 5.0

@interface DUBGraphicalEQView ()

@property (nonatomic, strong) NSArray *frequencyBars;

- (void)_sliderAction:(UISlider*)slider;

@end

#pragma mark -

@implementation DUBGraphicalEQView

@synthesize frequencyBars;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        
        NSUInteger drawnSliders = 0;
        UISlider *slider = nil;
        CGRect sliderFrame;
        
        for (NSNumber *frequency in FREQUENCY_TABLE) {
            
            float_t sliderOrigin = drawnSliders * (BAR_WIDTH + BAR_DISTANCE) + BAR_OFFSET;

            sliderFrame         = CGRectMake((frame.origin.x - 60) + sliderOrigin, frame.origin.y + 350, 150.0, 10.0);
            slider              = [[UISlider alloc] initWithFrame:sliderFrame];
            slider.transform    = CGAffineTransformRotate(slider.transform,270.0/180*M_PI);
            
            slider.minimumValue = -2.0;
            slider.maximumValue = 5.0;
            slider.continuous   = YES;
            slider.value        = 1.0;
            slider.tag          = [frequency integerValue];
            
            [slider addTarget:self action:@selector(_sliderAction:) forControlEvents:UIControlEventValueChanged];
            [slider setBackgroundColor:[UIColor clearColor]];
            [self addSubview:slider];
            [slider release];
            
            drawnSliders++;
        }
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext(); 
    
    CGContextFlush(context);
    
    NSUInteger drawnBars = 0;
    
    for (DUBFrequencyBar *bar in self.frequencyBars) {
        
        float_t barOrigin = drawnBars * (BAR_WIDTH + BAR_DISTANCE) + BAR_OFFSET;
            
        float_t color = bar.height / 200;
        
        if (!(drawnBars % 2)) {
            CGContextSetRGBFillColor(context, color, 0.0, 0.0, 1);
        } else if (!(drawnBars % 3)) {
            CGContextSetRGBFillColor(context, 0.0, color, 0.0, 1);
        } else {
            CGContextSetRGBFillColor(context, 0.0, 0.0, color, 1);
        }
        
        CGContextFillRect(context, CGRectMake(barOrigin, 250.0, BAR_WIDTH, -bar.height));
        
        drawnBars++;
    }
}

/*
 * NSSet<DUBFrequencyBar>
 */
- (void)drawBars:(NSSet*)bars {
    
        NSArray *sortDescs = [NSArray arrayWithObject:
                              [NSSortDescriptor sortDescriptorWithKey:@"frequency" 
                                                            ascending:YES]];
    
        self.frequencyBars = [bars sortedArrayUsingDescriptors:sortDescs];
    
        [self setNeedsDisplay];
}

- (void)_sliderAction:(UISlider*)slider {
   
    [((DUBAppDelegate*)[[UIApplication sharedApplication] delegate]).
        audio setValue:slider.value forFrequency:slider.tag];
}

@end
