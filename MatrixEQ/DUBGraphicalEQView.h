//
//  DUBGraphicalEQView.h
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DUBGraphicalEQView : UIView

/*
 * NSSet<DUBFrequencyBar>
 */
- (void)drawBars:(NSSet*)bars;

@end
