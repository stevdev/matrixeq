//
//  DUBViewController.h
//  GraphicalEQ
//
//  Created by steven reinisch on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "P_D_DUBAudioProcessingDelegate.h"

@interface DUBViewController : UIViewController <P_D_DUBAudioProcessingDelegate>

@end
